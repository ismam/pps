package u04lab.code

class ComplexImpl(override val re : Double, override val im : Double) extends Complex{

  override def +(c: Complex): Complex = {
    new ComplexImpl(re+c.re, im+c.im)
  }

  override def *(c: Complex): Complex = {
    new ComplexImpl ((re*c.re)-(im+c.im),(re+c.im)*(im+c.re))
  }
}
